package Utils;

public class ValidateLogin {
    String message = "";

    public boolean validate(String username, String password){

        if((username.equals(StringConfig.user_01[0])) && (password.equals(StringConfig.user_01[1]))){
            return true;
        }else{
            handleLoginErrors(password,username);
            return false;
        }
    }

    public String handleLoginErrors(String password, String username){
        if(username.length() == 0 || password.length()==0){
            message = StringConfig.popup_login_empty_message;
        }else if(username != StringConfig.user_01[0] || password != StringConfig.user_01[0]) {
            message = StringConfig.popup_login_invalid_message;
        }
        return message;
    }


}
