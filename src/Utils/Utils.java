package Utils;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    public void setDisplayName(JLabel jLabel){
        jLabel.setText(StringConfig.user_01[2]);
    }

    public void dateTimeManager(JLabel jLabelTime, JLabel jLabelDate){

        final DateFormat timeFormat, monthFormat, dayFormat, yearFormat;
        timeFormat = new SimpleDateFormat("HH:mm:ss");
        monthFormat = new SimpleDateFormat("M");
        dayFormat = new SimpleDateFormat("d");
        yearFormat = new SimpleDateFormat("yyyy");

        ActionListener timerListener = new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                Date date = new Date();
                String timeNow = timeFormat.format(date);
                int monthNow = Integer.parseInt(monthFormat.format(date));
                String dayNow = dayFormat.format(date);
                String yearNow = yearFormat.format(date);

                String month = StringConfig.months[monthNow - 1];

                jLabelTime.setText(timeNow);
                jLabelDate.setText(month + " " + dayNow + ", " + yearNow);
            }
        };

        Timer timer = new Timer(1000, timerListener);
        // to make sure it doesn't wait one second at the start
        timer.setInitialDelay(0);
        timer.start();
    }



}
