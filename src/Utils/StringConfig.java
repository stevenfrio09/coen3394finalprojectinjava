package Utils;

public class StringConfig {
    public static String title_login = "LoginWindow";

    public static String title_main = "Main";

    public static String[] user_01 = {"ADO", "AAA","Engr. Remedios G. Ado"};

    public static final String not_selectable_status = " - SELECT STATUS - ";

    public static String[] status = {"Present", "Absent", "In Official Business", "On Leave"};

    public static String[] months = {"January", "February", "March","April", "May", "June", "July", "August", "September", "October", "November", "December"};

    public static String popup_login_title = "LoginWindow Failed!";

    public static String popup_login_empty_message = "All fields are required.";

    public static String popup_login_invalid_message = "Invalid credentials.";

    public static String popup_update_status_title = "Update Status?";

    public static String popup_update_status_message = "Do you really want to update your status?";

    public static String popup_update_status_failed_title = "Update status failed!";

    public static String popup_update_status_failed_message = "Please update your status";

}

