import Utils.Utils;
import Utils.StringConfig;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainWindow {
    private JFrame jFrame;
    private GridBagConstraints gridBagConstraints;
    private JPanel jPanel;
    private JLabel jLabelName, jLabelTime, jLabelDate;
    private JButton jButton;
    private JComboBox jComboBox;

    public MainWindow(){
        createUIComponents();
        new Utils().setDisplayName(jLabelName);
        new Utils().dateTimeManager(jLabelTime, jLabelDate);
        onButtonClick();

    }

    private void createUIComponents() {
        jFrame = new JFrame(StringConfig.title_main);
        jFrame.setBounds(300,300,800,600);
        jFrame.setResizable(false);
        jFrame.setLocationRelativeTo(null);


        //init layout
        gridBagConstraints = new GridBagConstraints();

        //init panel
        jPanel = new JPanel(new GridBagLayout());
        jPanel.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));

        jLabelName = new JLabel();

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0;
        gridBagConstraints.insets = new Insets(5,0,5,0);
        gridBagConstraints.ipady = 5;
        jPanel.add(jLabelName, gridBagConstraints);

        jComboBox = new JComboBox();

        initComboBox();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0;
        gridBagConstraints.insets = new Insets(5,0,5,0);
        gridBagConstraints.ipady = 5;
        jPanel.add(jComboBox, gridBagConstraints);

        jLabelTime = new JLabel();

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0;
        gridBagConstraints.insets = new Insets(5,0,5,0);
        gridBagConstraints.ipady = 5;
        jPanel.add(jLabelTime, gridBagConstraints);


        jLabelDate = new JLabel();

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0;
        gridBagConstraints.insets = new Insets(5,0,5,0);
        gridBagConstraints.ipady = 5;
        jPanel.add(jLabelDate, gridBagConstraints);

        jButton = new JButton("Update");
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0;
        gridBagConstraints.insets = new Insets(5,0,5,0);
        gridBagConstraints.ipady = 5;


        jPanel.add(jButton, gridBagConstraints);

        jFrame.add(jPanel);
        jFrame.setVisible(true);
    }

    private void onButtonClick() {
        jButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String status = jComboBox.getSelectedItem().toString();
                if(status.equals(StringConfig.not_selectable_status)){
                    showErrorDialog();
                }else{
                    showUpdateDialog(status);
                }

            }
        });
    }

    private void showErrorDialog(){
        JOptionPane.showMessageDialog(null,StringConfig.popup_update_status_failed_message,
                StringConfig.popup_update_status_failed_title, JOptionPane.INFORMATION_MESSAGE);
    }


    private void showUpdateDialog(String status){
        int dialogResult = JOptionPane.showConfirmDialog(null,StringConfig.popup_update_status_message,
                StringConfig.popup_update_status_title, JOptionPane.YES_NO_OPTION);
        if(dialogResult ==  JOptionPane.YES_OPTION){
            new DisplayWindow(status);
            jFrame.setVisible(false);

        }else{

        }

    }


    public void initComboBox(){
        jComboBox.setModel(new DefaultComboBoxModel<String>() {
            private static final long serialVersionUID = 1L;
            boolean selectionAllowed = true;

            @Override
            public void setSelectedItem(Object anObject) {
                if (!StringConfig.not_selectable_status.equals(anObject)) {
                    super.setSelectedItem(anObject);
                } else if (selectionAllowed) {
                    // Allow this just once
                    selectionAllowed = false;
                    super.setSelectedItem(anObject);
                }
            }
        });

        jComboBox.addItem(StringConfig.not_selectable_status);
        jComboBox.addItem(StringConfig.status[0]);
        jComboBox.addItem(StringConfig.status[1]);
        jComboBox.addItem(StringConfig.status[2]);
        jComboBox.addItem(StringConfig.status[3]);
    }

    public static void main(String args[]){
        new MainWindow();
    }

}
