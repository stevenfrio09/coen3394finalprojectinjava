import Utils.StringConfig;
import Utils.ValidateLogin;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginWindow {

    private JFrame jFrame;
    private GridBagConstraints gridBagConstraints;
    private JPanel jPanel;
    private JLabel jLabelUName, jLabelPass;
    private JTextField jUsernameField;
    private JPasswordField jPasswordField;
    private JButton jButton;

    public LoginWindow(){
        createUIComponents();
        onButtonClick();
    }

    public void createUIComponents(){
        //init frame
        jFrame = new JFrame(StringConfig.title_login);
        jFrame.setBounds(300,300,800,600);
        jFrame.setResizable(false);
        jFrame.setLocationRelativeTo(null);

        //init layout
        gridBagConstraints = new GridBagConstraints();

        //init panel
        jPanel = new JPanel(new GridBagLayout());
        jPanel.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));

        jLabelUName = new JLabel("Username : ");
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0;
        gridBagConstraints.insets = new Insets(5,0,5,0);
        gridBagConstraints.ipady = 5;
        jPanel.add(jLabelUName, gridBagConstraints);

        jUsernameField = new JTextField();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(5,0,5,0);
        gridBagConstraints.ipady = 5;
        jPanel.add(jUsernameField, gridBagConstraints);

        jLabelPass = new JLabel("Password : ");
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.weightx = 0;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(5,0,5,0);
        gridBagConstraints.ipady = 5;
        jPanel.add(jLabelPass, gridBagConstraints);

        jPasswordField = new JPasswordField();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(5,0,5,0);
        gridBagConstraints.ipady = 5;
        jPanel.add(jPasswordField, gridBagConstraints);


        jButton = new JButton("Login");
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipady = 10;
        gridBagConstraints.insets = new Insets(5,0,5,0);
        gridBagConstraints.ipady = 5;
        jPanel.add(jButton, gridBagConstraints);


        jFrame.add(jPanel);
        jFrame.setVisible(true);

    }

    private void onButtonClick(){
        jButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String user = jUsernameField.getText().toString();
                String pass = jPasswordField.getText().toString();
                login(user, pass);
            }
        });
    }

    private void login(String username, String password){

        if(new ValidateLogin().validate(username, password)){
            new MainWindow();
            jFrame.setVisible(false);

        }else{
            showErrorDialog(username, password);
            clearLoginForm();
        }

    }

    private void showErrorDialog(String username, String password) {
        JOptionPane.showMessageDialog(null,
                new ValidateLogin().handleLoginErrors(username, password),
                StringConfig.popup_login_title, JOptionPane.INFORMATION_MESSAGE);
    }

    private void clearLoginForm(){
        jUsernameField.setText("");
        jPasswordField.setText("");
    }

    public static void main(String args[]){
        new LoginWindow();
    }
}
