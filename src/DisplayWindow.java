import Utils.Utils;
import Utils.StringConfig;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DisplayWindow {
    private JFrame jFrame;
    private GridBagConstraints gridBagConstraints;
    private JPanel jPanel;
    private JLabel jLabelName, jLabelStatus, jLabelTime, jLabelDate;
    private JButton jButtonUpdate;

    public DisplayWindow(String status){
        createUIComponents();
        new Utils().setDisplayName(jLabelName);
        new Utils().dateTimeManager(jLabelTime, jLabelDate);
        setStatus(status);
        onButtonClick();
    }

    private void createUIComponents(){
        jFrame = new JFrame(StringConfig.title_main);
        jFrame.setBounds(300,300,800,600);
        jFrame.setResizable(false);
        jFrame.setLocationRelativeTo(null);

        //init layout
        gridBagConstraints = new GridBagConstraints();

        //init panel
        jPanel = new JPanel(new GridBagLayout());
        jPanel.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));

        jLabelName = new JLabel();

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0;
        gridBagConstraints.insets = new Insets(5,0,5,0);
        gridBagConstraints.ipady = 5;
        jPanel.add(jLabelName, gridBagConstraints);

        jLabelStatus = new JLabel();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0;
        gridBagConstraints.insets = new Insets(5,0,5,0);
        gridBagConstraints.ipady = 5;
        jPanel.add(jLabelStatus, gridBagConstraints);

        jLabelTime = new JLabel();

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0;
        gridBagConstraints.insets = new Insets(5,0,5,0);
        gridBagConstraints.ipady = 5;

        jPanel.add(jLabelTime, gridBagConstraints);


        jLabelDate = new JLabel();

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0;
        gridBagConstraints.insets = new Insets(5,0,5,0);
        gridBagConstraints.ipady = 5;


        jPanel.add(jLabelDate, gridBagConstraints);

        jButtonUpdate = new JButton("Update");
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0;
        gridBagConstraints.insets = new Insets(5,0,5,0);
        gridBagConstraints.ipady = 5;

        jPanel.add(jButtonUpdate, gridBagConstraints);

        jFrame.add(jPanel);
        jFrame.setVisible(true);
    }

    private void onButtonClick() {
        jButtonUpdate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showUpdateDialog();
            }
        });
    }

    private void showUpdateDialog(){
        int dialogResult = JOptionPane.showConfirmDialog(null,StringConfig.popup_update_status_message,
                StringConfig.popup_update_status_title, JOptionPane.YES_NO_OPTION);
        if(dialogResult ==  JOptionPane.YES_OPTION){
            new LoginWindow();
            jFrame.setVisible(false);

        }else{
        }
    }

    private void setStatus(String status) {
        jLabelStatus.setText(status);
    }




    public static void main(String[] args, String status){
        new DisplayWindow(status);
    }
}
